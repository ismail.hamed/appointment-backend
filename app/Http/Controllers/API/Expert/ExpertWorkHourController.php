<?php


namespace App\Http\Controllers\API\Expert;


use App\Http\Controllers\Controller;
use App\Http\Requests\Expert\WorkHour\DeleteExpertWorkHourRequest;
use App\Http\Requests\Expert\WorkHour\StoreExpertWorkHourRequest;
use App\Http\Requests\Expert\WorkHour\UpdateExpertWorkHourRequest;
use App\Interfaces\Expert\ExpertWorkHourInterface;
use App\Models\Expert;
use App\Models\WorkHour;
use App\Transformers\WorkHourTransformer;

/**
 * @group Expert's work hours management
 *  APIs for CRUD expert's work hours
 */
class ExpertWorkHourController extends Controller
{
    protected $expertWorkHour;

    public function __construct(ExpertWorkHourInterface $expertWorkHour)
    {
        //todo WorkHourTransformer
        $this->middleware('auth:api')->except('index');
//        $this->middleware('transform.input:' . WorkHourTransformer::class)->except(['delete']);
        $this->expertWorkHour = $expertWorkHour;
    }

    /**
     * Get expert's work hours
     * This endpoint lets you to get work hours to experts
     * @responseFile storage/responses/expert/workHours/index.json
     * @return mixed
     */
    public function index(Expert $expert)
    {
        return $this->expertWorkHour->all($expert);
    }

    /**
     * Create expert's work hours
     * This endpoint lets you to create new work hours to experts
     * @responseFile storage/responses/expert/workHours/store.json
     * @return mixed
     */
    public function store(StoreExpertWorkHourRequest $request, Expert $expert)
    {
        return $this->expertWorkHour->store($expert);
    }

    /**
     * Update expert's work hour
     * This endpoint lets you to update existing work hour by identifier
     * @responseFile storage/responses/expert/workHours/update.json
     * @return mixed
     */
    public function update(UpdateExpertWorkHourRequest $request, Expert $expert, WorkHour $workHour)
    {
        return $this->expertWorkHour->update($workHour);
    }

    /**
     * delete expert's work hour
     * This endpoint lets you to delete existing work hour by identifier
     * @responseFile storage/responses/expert/workHours/delete.json
     * @return mixed
     */
    public function destroy(DeleteExpertWorkHourRequest $request, Expert $expert, WorkHour $workHour)
    {
        return $this->expertWorkHour->delete($workHour);
    }

}
