<?php

namespace App\Http\Requests\Expert\Booking;

use App\Enums\BookingDuration;
use App\Rules\ValidExpertDateTime;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * @bodyParam duration integer required duration  of the booking Example:15,30,45,60.
 * @bodyParam startAt date required date of the booking format must be Y-m-d H:i Example:1999-05-30 14:00.
 */
class UpdateExpertBookingRequest extends FormRequest
{
    //todo check if expert or admin or owner

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        //todo Uuid to user
        return [
            'duration' => ['required', Rule::in(BookingDuration::getValues())],
            'start_at' => [
                'required',
                'date',
                'date_format:Y-m-d H:i:s',
                'after:' . date('Y-m-d H:i'),
                new ValidExpertDateTime()
            ],
        ];

    }
}
