<?php

namespace App\Http\Requests\Expert\Booking;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class DeleteExpertBookingRequest extends FormRequest
{
    //todo check if expert
    public function authorize()
    {
        return Auth::user()->isAdmin();
    }


    public function rules()
    {
        return [
            //
        ];
    }
}
