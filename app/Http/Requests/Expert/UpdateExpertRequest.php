<?php

namespace App\Http\Requests\Expert;

use App\Enums\Countries;
use DateTimeZone;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

/**
 * @bodyParam firstName string first name of the expert.
 * @bodyParam lastName string  first name of the expert.
 * @bodyParam email email The email of the expert.
 * @bodyParam password string the desired password.
 * @bodyParam image file The image.
 * @bodyParam job string job of the expert.
 * @bodyParam country string country of the expert.
 * @bodyParam timeZone string time zone of the expert.
 */
class UpdateExpertRequest extends FormRequest
{

    public function authorize()
    {
        return Auth::user()->isAdmin();
    }


    public function rules()
    {
        $timeZones = DateTimeZone::listIdentifiers();
        $countries = Countries::getValues();
        return [
            'first_name' => ['max:255'],
            'last_name' => ['max:255'],
            'password' => ['min:6', 'max:255'],
            'image' => ['image', 'mimes:jpeg,png,jpg,gif,svg', 'max:2048'],
            'job' => ['max:255'],
            'country' => [Rule::in($countries)],
            'time_zone' => [Rule::in($timeZones)],

        ];
    }
}
