<?php

namespace App\Http\Requests\Expert;

use App\Rules\ValidExpertDateTime;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @bodyParam date date required date of booking format must be Y-m-d.
 */
class GetAvailabilityTimeRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'date' => [
                'required',
                'date',
                'date_format:Y-m-d',
                'after_or_equal:today',
            ],
        ];
    }
}
